package net.therap.mealmanager.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author subrata
 * @since 11/24/16
 */
@WebServlet("/mealmanager/login")
public class LoginServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/view/login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        String email = request.getParameter("email");
        String password = request.getParameter("password");


        if (email != null && password != null) {

            session.setAttribute("email", email);
            session.setAttribute("password", password);

            response.sendRedirect("/mealmanager/mainmenu");

        } else {
            request.getRequestDispatcher("/WEB-INF/view/login.jsp").forward(request, response);
        }

    }
}
